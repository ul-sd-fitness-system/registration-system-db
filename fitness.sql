Alter USER 'root'@'localhost'identified with mysql_native_password BY ''

CREATE TABLE `fitness`.`fitness_member` (
  `fitness_member_id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `dob` DATETIME NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(13) NOT NULL,
  `type` VARCHAR(45) NULL,
  `memberType` VARCHAR(45) NULL,
  PRIMARY KEY (`fitness_member_id`));

ALTER TABLE `fitness`.`fitness_member` 
CHANGE COLUMN `dob` `dob` VARCHAR(20) NOT NULL ;

ALTER TABLE `fitness`.`fitness_member` 
CHANGE COLUMN `fitness_member_id` `fitness_member_id_id` INT(11) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `fitness`.`fitness_member` 
CHANGE COLUMN `fitness_member_id_id` `fitness_member_id` INT(11) NOT NULL AUTO_INCREMENT ;

CREATE TABLE `fitness`.`location` (
  `location_id` INT NOT NULL AUTO_INCREMENT,
  `location_name` VARCHAR(45) NOT NULL,
  `location_address` VARCHAR(45) NULL,
  PRIMARY KEY (`location_id`));

CREATE TABLE `fitness`.`Instructor` (
  `instructor_id` INT NOT NULL AUTO_INCREMENT,
  `instructor_name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `facility_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`instructor_id`));
  


CREATE TABLE `fitness`.`facility` (
 `facility_id` INT NOT NULL AUTO_INCREMENT,
 `facility_type` VARCHAR(45) NOT NULL,
 PRIMARY KEY (`facility_id`));
 
 
 
 CREATE TABLE `freeBooking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `classType` varchar(45) DEFAULT NULL,
  `instructor_name` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

 
 CREATE TABLE `personalBooking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `classType` varchar(45) DEFAULT NULL,
  `instructor_name` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

 
 CREATE TABLE `memberBooking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `classType` varchar(45) DEFAULT NULL,
  `instructor_name` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

